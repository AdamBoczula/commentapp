# Rate & Comments

This is application to rate and comment some product

There will be select with products, stars with rate, textarea with comment, and input with e-mail address.

Product, rating and comment is required, but e-mail is not required.

Rating is disabled from the beginning, but after product is selected, then ratings is enabled, and user should rate that product.


## Stack

* NgRx,
  * store,
  * effect,
  * router,
* Bootstrap (without components),
* material CDK,


## Requirements

Products selector should be contains autocomplete feature.

Every file should be tested with unit.

Try to write e2e test with protractor.

After any changes in form, data should be set in local storage (by effects). It should be clear after add comment. When user refresh page with not end rating it should restore data.



## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
