import { TestBed } from '@angular/core/testing';

import { CommentsService } from './comments.service';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { LocalStorageService } from './local-storage.service';
import { Comment } from '../models';
import { commentsAction } from '../store/actions';

describe('CommentsService', () => {
  const initialState = { comments: [], error: null };
  let service: CommentsService;
  let store: MockStore<any>;
  let lsMock;

  beforeEach(() => {
    lsMock = jasmine.createSpyObj('lsMock', ['add', 'fetch']);

    TestBed.configureTestingModule({
      providers: [
        provideMockStore({ initialState }),
        { provide: LocalStorageService, useValue: lsMock }
      ]
    });
    service = TestBed.inject(CommentsService);
    store = TestBed.inject(MockStore);
    store.setState({});
    spyOn(store, 'dispatch').and.callFake(() => {});

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addComment should call LocalStorageService add method once', () => {
    const comment: Comment = {
      email: 'abc@example',
      comment: 'bla bla bla',
      product: { text: 'ProductX', id: '0' },
      rate: 3
    };
    service.addComment(comment);

    expect(lsMock.add).toHaveBeenCalledOnceWith('comments', comment);
  });

  it('fetchComments should call LocalStorageService fetch method once', () => {
    service.fetchComments();
    expect(lsMock.fetch).toHaveBeenCalledOnceWith('comments');
  });

  it('dispatchFetchComments should dispatch fetchComments', () => {
    service.dispatcFetchComments();
    expect(store.dispatch).toHaveBeenCalledOnceWith(
      commentsAction.fetchComments()
    );
  });
});
