import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  constructor() { }

  public add(key: string, value: any): void {
    const lsItems: string = localStorage.getItem(key);
    const items: any = JSON.parse(lsItems ?? '[]');

    items.push(value);

    localStorage.setItem(key, JSON.stringify(items));
  }

  public fetch(key: string): any {
    const item: string = localStorage.getItem(key);

    return item ? JSON.parse(item) : null;
  }
}
