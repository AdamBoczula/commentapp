import { TestBed } from '@angular/core/testing';

import { LocalStorageService } from './local-storage.service';

describe('LocalStorageService', () => {
  let service: LocalStorageService;
  let store;

  beforeEach(() => {
    store = {};

    spyOn(localStorage, 'getItem').and.callFake((key) => JSON.stringify(store[key]));
    spyOn(localStorage, 'setItem').and.callFake((value, key) => store[key] = value);

    TestBed.configureTestingModule({});
    service = TestBed.inject(LocalStorageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('add should call getItem and setItem once', () => {
    const key = 'key';
    const value = { foo: 'bar' };

    service.add(key, value);
    expect(localStorage.getItem).toHaveBeenCalledOnceWith(key);
    expect(localStorage.setItem).toHaveBeenCalledTimes(1);
  });

  it('add should create new localstorage object', () => {
    const key = 'key';
    const value = { foo: 'bar' };

    service.add(key, value);
    expect(localStorage.setItem).toHaveBeenCalledWith(key, JSON.stringify([value]));
  });

  it('add should add element localstorage list', () => {
    const oldValue = { foo: 'baz' };
    store.key = [oldValue];
    const key = 'key';
    const value = { foo: 'bar' };

    service.add(key, value);
    expect(localStorage.setItem).toHaveBeenCalledWith(key, JSON.stringify([oldValue, value]));
  });

  it('fetch should get localstorage key once', () => {
    const key = 'key';
    service.fetch(key);
    expect(localStorage.getItem).toHaveBeenCalledWith(key);
  });

  it('fetch should get localstorage parsed elements', () => {
    store.key = [{ fazi: 'bazi' }];
    const key = 'key';
    const elements = service.fetch(key);

    expect(elements).toEqual(store.key);
  });
});
