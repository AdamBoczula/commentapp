import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Comment } from '../models';
import { selectComments } from '../reducers';
import { LocalStorageService } from './local-storage.service';
import { commentsAction } from '../store/actions';

const commentsKey = 'comments';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {
  comments$: Observable<Comment[]> = this.store.select(selectComments);

  constructor(private store: Store, private localStorageService: LocalStorageService) { }

  public addComment(comment: Comment): void {
    this.localStorageService.add(commentsKey, comment);
  }

  public fetchComments(): Comment[] {
    return this.localStorageService.fetch(commentsKey) as Comment[];
  }

  public dispatcFetchComments(): void {
    return this.store.dispatch(commentsAction.fetchComments());
  }
}
