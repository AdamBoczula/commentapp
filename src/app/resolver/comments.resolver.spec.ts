import { TestBed } from '@angular/core/testing';

import { CommentsResolver } from './comments.resolver';
import createSpyObj = jasmine.createSpyObj;
import { CommentsService } from '../service/comments.service';

describe('CommentsResolver', () => {
  let csMock;
  let resolver: CommentsResolver;

  beforeEach(() => {
    csMock = createSpyObj('commentsService', ['dispatcFetchComments']);
    TestBed.configureTestingModule({
        providers: [
          { provide: CommentsService, useValue: csMock },
        ]
      }
    );
    resolver = TestBed.inject(CommentsResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  it('resolver should call fetchComments in commentsService', () => {
    resolver.resolve();
    expect(csMock.dispatcFetchComments).toHaveBeenCalledTimes(1);
  });
});
