import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { CommentsService } from '../service/comments.service';

@Injectable({
  providedIn: 'root'
})
export class CommentsResolver implements Resolve<boolean> {
  constructor(private commentsService: CommentsService) {}

  resolve(): boolean {
    this.commentsService.dispatcFetchComments();
    return true;
  }
}
