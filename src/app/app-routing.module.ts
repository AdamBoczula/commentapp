import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RootCommentsComponent } from './containers/root-comments/root-comments.component';
import { CommentsResolver } from './resolver/comments.resolver';

const routes: Routes = [
  { path: '', component: RootCommentsComponent, resolve: [CommentsResolver] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
