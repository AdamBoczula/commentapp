import { ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer } from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromComments from '../store/reducers/comments.reducer';
import { commentsFeatureKey } from '../store/reducers/comments.reducer';

export interface State {
  [fromComments.commentsFeatureKey]: fromComments.State;
}

export const reducers: ActionReducerMap<{}> = {
  [fromComments.commentsFeatureKey]: fromComments.reducer
};

export function logger(reducer: ActionReducer<{}>): ActionReducer<{}> {
  return (state, action) => {
    const result = reducer(state, action);
    console.groupCollapsed(action.type);
    console.log('prev state', state);
    console.log('action', action);
    console.log('next state', result);
    return result;
  };
}

export const metaReducers: MetaReducer<{}>[] = !environment.production
  ? [logger]
  : [];

export const selectCommentState = createFeatureSelector<State, fromComments.State>(commentsFeatureKey);

export const selectComments = createSelector(
  selectCommentState,
  (state) => state.comments
);
