import { createAction, props } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { Comment } from '../../models';

export const fetchComments = createAction('[Comment] Fetch Comments');
export const fetchCommentsSuccess = createAction('[API/CommentCreation] Fetch Comments Success',
  props<{ comments: Comment[] }>());
export const fetchCommentsFailure = createAction('[API/CommentCreation] Fetch Comments Failure',
  props<{ error: HttpErrorResponse }>());
export const addComment = createAction('[CommentsCreation] Add Comment', props<{ comment: Comment }>());
export const addCommentSuccess = createAction('[API/CommentCreation] Add Comment Success');
export const addCommentFailure = createAction('[API/CommentCreation] Add Comment Failure', props<{ error: HttpErrorResponse }>());

