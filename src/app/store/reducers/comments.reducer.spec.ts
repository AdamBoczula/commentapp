import * as fromComments from './comments.reducer';
import { commentsAction } from '../actions';
import { Comment } from '../../models';
import { HttpErrorResponse } from '@angular/common/http';

describe('CommentsReducer', () => {
  const initialState = { comments: [], error: null };

  it('addComment should add comment to comment list', () => {
    const comment: Comment = {
      comment: 'comment',
      email: 'example@email.com',
      product: { id: '0', text: 'productA' },
      rate: 4
    };
    const action = commentsAction.addComment({
      comment
    });
    const state = fromComments.reducer(initialState, action);

    expect(state).toEqual({ ...state, comments: [comment] }, 'comment should be added to the store');
  });

  it('fetchCommentsSuccess should attach list of comments', () => {
    const comments: Comment[] = [{
      comment: 'comment',
      email: 'example@email.com',
      product: { id: '0', text: 'productA' },
      rate: 4
    }, {
      comment: 'comment',
      email: 'example@email.com',
      product: { id: '0', text: 'productB' },
      rate: 2
    }];
    const action = commentsAction.fetchCommentsSuccess({ comments });
    const state = fromComments.reducer(initialState, action);
    expect(state).toEqual({ ...state, comments }, 'comments should be added to the store');

  });
  it('addCommentFailure should attach error', () => {
    const error: HttpErrorResponse = {
      headers: undefined,
      message: 'msg',
      name: 'HttpErrorResponse',
      ok: false,
      status: 0,
      statusText: '',
      type: undefined,
      url: undefined,
      error: 'abc'
    };
    const action = commentsAction.addCommentFailure({ error });
    const state = fromComments.reducer(initialState, action);
    expect(state).toEqual({ ...state, error }, 'error should be added to the store');
  });
  it('fetchCommentsFailure should attach error', () => {
    const error: HttpErrorResponse = {
      headers: undefined,
      message: 'msg',
      name: 'HttpErrorResponse',
      ok: false,
      status: 0,
      statusText: '',
      type: undefined,
      url: undefined,
      error: 'abc'
    };
    const action = commentsAction.fetchCommentsFailure({ error });
    const state = fromComments.reducer(initialState, action);
    expect(state).toEqual({ ...state, error }, 'error should be added to the store');
  });
});
