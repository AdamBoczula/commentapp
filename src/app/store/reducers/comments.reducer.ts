import { HttpErrorResponse } from '@angular/common/http';
import { Comment } from '../../models';
import { createReducer, on } from '@ngrx/store';
import * as commentsAction from '../actions/comments-actions';

export const commentsFeatureKey = 'comments';

export interface State {
  comments: Comment[];
  error: HttpErrorResponse;
}

const initialState: State = {
  comments: [],
  error: null,
};

export const reducer = createReducer(
  initialState,
  on(commentsAction.fetchCommentsSuccess, (state, { comments }) => (
    {
      ...state,
      comments
    }
  )),
  on(commentsAction.addComment, (state, { comment }) => (
    {
      ...state,
      comments: [...state.comments, comment],
    }
  )),
  on(
    commentsAction.addCommentFailure,
    commentsAction.fetchCommentsFailure,
    (state, { error }) => ({ ...state, error }))
);

