// import * as fromComments from './comments.reducer';
// import { commentsFeatureKey } from './comments.reducer';
// import * as fromRoot from '../../reducers';
// import { Action, combineReducers, createFeatureSelector, createSelector } from '@ngrx/store';
//
// export const appFeatureKey = 'app';
//
// export interface AppState {
//   [fromComments.commentsFeatureKey]: fromComments.State;
// }
//
// export interface State extends fromRoot.State {
//   [appFeatureKey]: AppState;
// }
//
// export function reducers(state: AppState | undefined, action: Action): {
//   [fromComments.commentsFeatureKey]: fromComments.State
// } {
//   return combineReducers({
//     [fromComments.commentsFeatureKey]: fromComments.reducer
//   })(state, action);
// }
//
// export const selectAppState = createFeatureSelector<State, AppState>(
//   appFeatureKey
// );
//
// export const selectCommentsState = createSelector(
//   selectAppState,
//   (state) => state[commentsFeatureKey]
// );
//
