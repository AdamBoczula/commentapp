import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { commentsAction } from '../actions';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { CommentsService } from '../../service/comments.service';

@Injectable()
export class CommentsEffect {
  public fetchComments$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(commentsAction.fetchComments),
      map(() => {
        const comments = this.commentsService.fetchComments();
        return commentsAction.fetchCommentsSuccess({ comments });
      })
    )
  );

  public addComment$: Observable<Action> = createEffect(() =>
    this.actions$.pipe(
      ofType(commentsAction.addComment),
      map(({ comment }) => {
        this.commentsService.addComment(comment);
        return commentsAction.addCommentSuccess();
      })
    )
  );

  constructor(private actions$: Actions, private commentsService: CommentsService) {}
}
