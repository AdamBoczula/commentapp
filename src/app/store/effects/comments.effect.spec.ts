import { TestBed } from '@angular/core/testing';
import { CommentsEffect } from './comments.effect';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { Observable } from 'rxjs';
import { TestScheduler } from 'rxjs/testing';
import { addComment, addCommentSuccess, fetchComments, fetchCommentsSuccess } from '../actions/comments-actions';
import { Comment } from '../../models';
import { CommentsService } from '../../service/comments.service';

describe('CommentsEffects', () => {
  const comment: Comment =
    {
      id: '0',
      product: { id: '0', text: 'ProductX' },
      email: 'example@email.com',
      comment: 'comment',
      rate: 4
    };
  const comments: Comment[] = [comment];
  const initialState = { comments: [], error: null };
  let csMock;
  let actions$: Observable<any>;
  let effects: CommentsEffect;
  let store: MockStore<any>;
  let testScheduler;

  beforeEach(() => {
    csMock = jasmine.createSpyObj('commentsService', ['addComment',]);
    csMock.fetchComments = () => {};
    spyOn(csMock, 'fetchComments').and.returnValue(comments);

    TestBed.configureTestingModule({
      providers: [
        CommentsEffect,
        provideMockStore({ initialState }),
        provideMockActions(() => actions$),
        { provide: CommentsService, useValue: csMock },
      ]
    });
    effects = TestBed.inject(CommentsEffect);
    store = TestBed.inject(MockStore);
    store.setState({});

    testScheduler = new TestScheduler((actual, expected) => {
      expect(actual).toEqual(expected);
    });
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('addComment$', () => {
    it('should handle addComment and return addCommentSuccess action', () => {
      const action = addComment({ comment });
      testScheduler.run(({ hot, cold, expectObservable }) => {
        actions$ = hot('1ms a', { a: action });
        const expected$ = '1ms b';
        expectObservable(effects.addComment$).toBe(expected$, { b: addCommentSuccess() });
      });
    });
  });
  describe('fetchComments$', () => {
    it('should handle fetchComment and return fetchCommentSuccess action', () => {

      const action = fetchComments();
      testScheduler.run(({ hot, cold, expectObservable }) => {
        actions$ = hot('1ms a', { a: action });
        const expected$ = '1ms b';
        expectObservable(effects.fetchComments$).toBe(expected$, { b: fetchCommentsSuccess({ comments }) });
      });
    });
  });
});
