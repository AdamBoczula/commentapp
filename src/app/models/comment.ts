import { Product } from './';

export interface Comment {
  id?: string;
  product: Product;
  rate: number;
  email: string;
  comment: string;
}

// product: [null, Validators.required],
//   rate: [{ value: 0, disabled: true }, [Validators.required, Validators.min(1), Validators.max(this.maxRate)]],
//   email: ['', Validators.email],
//   comment: ['', Validators.maxLength(999)]
