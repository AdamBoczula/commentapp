import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SelectItem } from '../../../ui/select/models';
import { Store } from '@ngrx/store';
import { commentsAction } from '../../store/actions/';
import { Comment } from '../../models';

@Component({
  selector: 'app-rate-form',
  templateUrl: './rate-form.component.html',
  styleUrls: ['./rate-form.component.scss']
})
export class RateFormComponent {
  public maxRate = 5;
  public form: FormGroup = this.fb.group({
    product: [null, Validators.required],
    rate: [{ value: 0, disabled: true }, [Validators.required, Validators.min(1), Validators.max(this.maxRate)]],
    email: [null, Validators.email],
    comment: [null, Validators.maxLength(999)]
  });

  public items: SelectItem[] = [
    { id: '0', text: 'foo' }, { id: '1', text: 'bar' }
  ];

  constructor(private fb: FormBuilder, private store: Store) {
    this.productFC.valueChanges.subscribe(product => {
      if (!product) {
        this.rateFC.disable();
      } else {
        this.rateFC.enable();
      }
    });
  }

  public get rateFC(): AbstractControl {
    return this.form.get('rate');
  }

  public get productFC(): AbstractControl {
    return this.form.get('product');
  }

  public addComment(): void {
    if (this.form.valid) {
      this.store.dispatch(commentsAction.addComment(
        { comment: this.form.value as Comment })
      );
      this.form.reset();
    }
  }

  public clearForm(): void {
    this.productFC.reset();
    this.rateFC.reset();
  }
}
