import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateFormComponent } from './rate-form.component';
import { AppModule } from '../../app.module';
import { By } from '@angular/platform-browser';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { Comment } from '../../models';
import { commentsAction } from '../../store/actions';

describe('RateFormComponent', () => {
  const initialState = { error: null, comments: [] };
  let component: RateFormComponent;
  let fixture: ComponentFixture<RateFormComponent>;
  let store: MockStore;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RateFormComponent],
      imports: [AppModule],
      providers: [
        provideMockStore({ initialState })
      ]
    })
      .compileComponents();

    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(store, 'dispatch').and.callFake(() => {});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('form', () => {
    let productFC;
    let rateFC;
    let emailFC;
    let commentFC;

    beforeEach(() => {
      productFC = component.form.get('product');
      rateFC = component.form.get('rate');
      emailFC = component.form.get('email');
      commentFC = component.form.get('comment');
      component.form.patchValue({
        product: 'dummy product',
        rate: 1,
        email: 'example@email.com',
        comment: null
      });
    });

    describe('validators', () => {
      it('should be INVALID when product is not selected', () => {
        productFC.setValue(null);
        fixture.detectChanges();
        expect(component.form.invalid).toBeTrue();
      });
      it('should be VALID when product is selected', () => {
        productFC.setValue('ProductX');
        fixture.detectChanges();
        expect(component.form.valid).toBeTrue();
      });
      it('should be INVALID when rate is not selected', () => {
        rateFC.setValue(null);
        fixture.detectChanges();
        expect(component.form.invalid).toBeTrue();
      });
      it('should be INVALID when rate below 0', () => {
        rateFC.setValue(0);
        fixture.detectChanges();
        expect(component.form.invalid).toBeTrue();
      });
      it('should be INVALID when rate is more 6', () => {
        rateFC.setValue(6);
        fixture.detectChanges();
        expect(component.form.invalid).toBeTrue();
      });
      it('should be VALID when rate is selected', () => {
        rateFC.setValue(1);
        fixture.detectChanges();
        expect(component.form.valid).toBeTrue();
      });
      it('should be VALID when email is not provided', () => {
        emailFC.setValue(null);
        fixture.detectChanges();
        expect(component.form.valid).toBeTrue();
      });
      it('should be VALID when comment is empty', () => {
        commentFC.setValue(null);
        fixture.detectChanges();
        expect(component.form.valid).toBeTrue();
      });
      it('should be VALID when comment is not empty', () => {
        const comment = 'x'.repeat(999);
        commentFC.setValue(comment);
        fixture.detectChanges();
        expect(component.form.valid).toBeTrue();
      });
      it('should be INVALID when comment contains more characters than \'999\'', () => {
        const tooLongComment = 'x'.repeat(1000);
        commentFC.setValue(tooLongComment);
        fixture.detectChanges();
        expect(component.form.invalid).toBeTrue();
      });
    });
    it('rate stars should be disabled by default', () => {
      productFC.patchValue(null);
      fixture.detectChanges();
      expect(rateFC.disabled).toBeTrue();
    });
    it('rate stars should be enabled after select product', () => {
      expect(rateFC.disabled).toBeFalse();
    });
    it('remove should clear form', () => {
      component.clearForm();
      fixture.detectChanges();
      expect(rateFC.value).toBeNull();
      expect(productFC.value).toBeNull();
    });
    describe('button', () => {
      let buttonEl;
      beforeEach(() => {
        buttonEl = fixture.debugElement.query(By.css('.btn')).nativeElement;
      });
      it('should be disabled when form is invalid', () => {
        rateFC.patchValue(0);
        fixture.detectChanges();
        expect(buttonEl.disabled).toBeTrue();
      });
      it('should be enabled when form is valid', () => {
        fixture.detectChanges();
        expect(buttonEl.disabled).toBeFalse();
      });
      it('should dispatch createComment event when form is valid', () => {
        const comment = {
          product: productFC.value,
          rate: rateFC.value,
          email: emailFC.value,
          comment: commentFC.value
        } as Comment;

        component.addComment();
        fixture.detectChanges();

        expect(store.dispatch).toHaveBeenCalledOnceWith(
          commentsAction.addComment({ comment })
        );
      });
      it('icon-wrapper should be removed after clicked', () => {
        component.addComment();
        fixture.detectChanges();
        const removeIcon = fixture.debugElement.query(By.css('.icon-wrapper'));
        expect(removeIcon).toBeNull();
      });
      it('should not dispatch createComment event when form is invalid', () => {
        rateFC.patchValue(null);
        component.addComment();
        fixture.detectChanges();

        expect(store.dispatch).not.toHaveBeenCalled();
      });
      it('should clear form after click', () => {
        component.addComment();
        fixture.detectChanges();

        expect(rateFC.value).toBeFalsy();
        expect(emailFC.value).toBeFalsy();
        expect(commentFC.value).toBeFalsy();
        expect(productFC.value).toBeFalsy();
      });
    });
  });
});
