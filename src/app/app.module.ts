import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RateFormComponent } from './components/rate-form/rate-form.component';
import { RateWithCommentComponent } from './containers/rate-with-comment/rate-with-comment.component';
import { ReactiveFormsModule } from '@angular/forms';
import { UiModule } from '../ui/ui.module';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { CommentsEffect } from './store/effects/comments.effect';
import { CommentsListComponent } from './containers/comments-list/comments-list.component';
import { CommentComponent } from './containers/comment/comment.component';
import { AppRoutingModule } from './app-routing.module';
import { RootCommentsComponent } from './containers/root-comments/root-comments.component';

@NgModule({
  declarations: [
    AppComponent,
    RateFormComponent,
    RateWithCommentComponent,
    CommentsListComponent,
    CommentComponent,
    RootCommentsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    UiModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ name: 'NgRx Comment App' }),
    EffectsModule.forRoot([CommentsEffect]),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
