import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

@NgModule({
  declarations: [],
  imports: [
    OverlayModule,
    CommonModule
  ],
  exports: [OverlayModule]
})
export class MaterialModule {
}
