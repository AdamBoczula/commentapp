import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RateWithCommentComponent } from './rate-with-comment.component';
import { Component } from '@angular/core';
import { AppModule } from '../../app.module';

describe('RateWithCommentComponent', () => {
  let component: RateWithCommentComponent;
  let fixture: ComponentFixture<RateWithCommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RateWithCommentComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RateWithCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
