import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RootCommentsComponent } from './root-comments.component';
import { AppModule } from '../../app.module';

describe('RootCommentsComponent', () => {
  let component: RootCommentsComponent;
  let fixture: ComponentFixture<RootCommentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RootCommentsComponent],
      imports: [AppModule],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RootCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
