import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import { Component } from '@angular/core';
import { Comment } from '../../models';
import { RateStarsComponent } from '../../../ui/rate-stars/rate-stars.component';

describe('CommentComponent', () => {
  let component: TestCommentComponent;
  let fixture: ComponentFixture<TestCommentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TestCommentComponent, CommentComponent, RateStarsComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

@Component({
  selector: 'test-comment',
  template: `
    <app-comment
      [comment]="comment">
    </app-comment>
  `
})
class TestCommentComponent {
  public comment: Comment = {
    product: { id: '0', text: 'ProductX' },
    email: 'example@email',
    comment: 'Abc abc abc',
    rate: 3,
  };

}
