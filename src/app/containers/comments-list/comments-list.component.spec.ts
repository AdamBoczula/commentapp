import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsListComponent } from './comments-list.component';
import { CommentsService } from '../../service/comments.service';
import { BehaviorSubject } from 'rxjs';
import { Comment } from '../../models';
import { By } from '@angular/platform-browser';
import { CommentComponent } from '../comment/comment.component';

let commentServiceMock;

describe('CommentsListComponent', () => {
  let component: CommentsListComponent;
  let fixture: ComponentFixture<CommentsListComponent>;
  let commentsElement;

  beforeEach(async () => {
    commentServiceMock = jasmine.createSpyObj(['x'], []);
    commentServiceMock.comments$ = new BehaviorSubject([{
      rate: 1,
      comment: 'abc',
      email: 'abc@a',
      product: { text: 'ProductX', id: '0' }
    } as Comment,
      {
        rate: 4,
        comment: 'comm1',
        email: 'gdf@example.com',
        product: { text: 'ProductY', id: '0' }
      } as Comment]);

    await TestBed.configureTestingModule({
      declarations: [CommentsListComponent, CommentComponent],
      providers: [
        { provide: CommentsService, useValue: commentServiceMock }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    commentsElement = fixture.debugElement.queryAll(By.css('app-comment'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('comments exists', () => {
    expect(commentsElement.length).toEqual(2);
  });
});
