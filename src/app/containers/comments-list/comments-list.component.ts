import { Component } from '@angular/core';
import { CommentsService } from '../../service/comments.service';

@Component({
  selector: 'app-comments-list',
  templateUrl: './comments-list.component.html',
  styleUrls: ['./comments-list.component.scss']
})
export class CommentsListComponent {
  public comments$ = this.commentsService.comments$;

  constructor(private commentsService: CommentsService) { }

}
