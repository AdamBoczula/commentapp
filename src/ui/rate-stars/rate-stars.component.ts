import { Component, forwardRef, Input, OnChanges, Optional, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-rate-stars',
  templateUrl: './rate-stars.component.html',
  styleUrls: ['./rate-stars.component.scss'],
  providers: [
    {
      useExisting: forwardRef(() => RateStarsComponent),
      provide: NG_VALUE_ACCESSOR,
      multi: true,
    }
  ]
})
export class RateStarsComponent implements ControlValueAccessor, OnChanges {
  @Input() public staticRate: number;
  @Input() private maxRate = 5;
  public stars: number[];
  public highlightedRate: number;
  public disabled = true;
  public onChange: (value: any) => {};
  public onTouch: () => {};

  // tslint:disable-next-line:variable-name
  private _rate: number;

  public set rate(rate: number) {
    this._rate = rate;
  }

  public get rate(): number {
    return this._rate;
  }

  // tslint:disable-next-line:variable-name
  constructor(private _renderer: Renderer2) {
    this.createStars();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    const staticRate: number = changes.staticRate?.currentValue;

    if (changes.maxRate?.firstChange) {
      this.createStars();
    }

    if (changes.staticRate?.currentValue > 0) {
      this.disabled = false;
      this.rate = staticRate;
    }
  }

  private createStars(): void {
    this.stars = Array(this.maxRate).fill(0).map((n, i) => i + 1);
  }

  public registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(newRate: number): void {
    this.rate = newRate;
  }

  public mouseenter(rate: number): void {
    if (this.staticRate) {
      return;
    }

    this.highlightedRate = rate;
  }

  public resetRate(): void {
    this.highlightedRate = null;
  }

  public setRate(newRate: number): void {
    if (this.disabled || this.staticRate) {
      return;
    }

    this.rate = newRate;
    this.onChange(this.rate);
    this.onTouch();
  }

  public shouldBeHighlighted(star: number): boolean {
    if (this.disabled) {
      return false;
    }

    if (this.highlightedRate) {
      return this.highlightedRate >= star;
    }
    return this.rate >= star;
  }
}
