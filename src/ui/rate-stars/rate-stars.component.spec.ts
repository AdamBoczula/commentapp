import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';

import { RateStarsComponent } from './rate-stars.component';
import { Component, ViewChild } from '@angular/core';
import { UiModule } from '../ui.module';
import { FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

describe('RateStarsComponent', () => {
  describe('component with formControl', () => {
    let component: TestRateStarsFCComponent;
    let fixture: ComponentFixture<TestRateStarsFCComponent>;
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        declarations: [TestRateStarsFCComponent],
        imports: [UiModule, FormsModule, ReactiveFormsModule]
      })
        .compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(TestRateStarsFCComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.rateStarsComponent.disabled = false;
    });

    it('should create', () => {
      expect(component.rateStarsComponent).toBeTruthy();
    });

    it('should display max rate number of stars', () => {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      expect(starsEl.length).toEqual(component.maxRate);
    });

    it('should add \'accepted-rate\' class when mouseenter', () => {
      component.rateStarsComponent.disabled = false;
      fixture.detectChanges();
      checkStarsHighlight(true, 3);
    });

    it('when disabled should not highlight star', () => {
      component.rateStarsComponent.disabled = true;
      fixture.detectChanges();
      checkStarsHighlight(false, 0);
    });

    it('should remove \'accepted-rate\' class when mouseover stars container', () => {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      const starsContainerEl = fixture.debugElement.query(By.css('.stars'));
      starsEl[2].nativeNode.dispatchEvent(new MouseEvent('mouseenter'));
      fixture.detectChanges();
      starsContainerEl.nativeElement.dispatchEvent(new MouseEvent('mouseout'));
      fixture.detectChanges();
      const highlightedStars = fixture.debugElement.queryAllNodes(By.css('.accepted-rate'));
      expect(highlightedStars.length).toEqual(0);
    });

    describe('after rate', () => {
      it('should set rate on formControl', () => {
        component.form.get('rateFC').patchValue(3);
        fixture.detectChanges();
        expect(component.form.get('rateFC').value).toBe(3);
      });

      it('should stay highlight after mouseout', () => {
        const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
        const starsContainerEl = fixture.debugElement.query(By.css('.stars'));

        // mouse enter
        starsEl[2].nativeNode.dispatchEvent(new MouseEvent('mouseenter'));
        fixture.detectChanges();
        // set rate
        component.rateStarsComponent.writeValue(3);
        fixture.detectChanges();
        // mouse out
        starsContainerEl.nativeElement.dispatchEvent(new MouseEvent('mouseout'));
        fixture.detectChanges();
        const highlightedStars = fixture.debugElement.queryAllNodes(By.css('.accepted-rate'));
        expect(highlightedStars.length).toEqual(3);
      });
    });

    function checkStarsHighlight(result: boolean, numberOfHighlightedStar: number): void {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      starsEl[2].nativeNode.dispatchEvent(new MouseEvent('mouseenter'));
      fixture.detectChanges();
      const highlightedStars = fixture.debugElement.queryAllNodes(By.css('.accepted-rate'));
      expect(highlightedStars.length).toEqual(numberOfHighlightedStar);
      expect(starsEl[0].nativeNode.classList.contains('accepted-rate')).toBe(result, 'first star should be highlighted');
      expect(starsEl[1].nativeNode.classList.contains('accepted-rate')).toBe(result, 'second star should be highlighted');
      expect(starsEl[2].nativeNode.classList.contains('accepted-rate')).toBe(result, 'third star should be highlighted');
    }
  });

  describe('component with input', () => {
    let component: TestRateStarsComponent;
    let fixture: ComponentFixture<TestRateStarsComponent>;
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        declarations: [TestRateStarsComponent],
        imports: [UiModule, FormsModule, ReactiveFormsModule]
      })
        .compileComponents();
    });

    beforeEach(() => {
      fixture = TestBed.createComponent(TestRateStarsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      component.rateStarsComponent.disabled = false;
    });

    it('should create', () => {
      expect(component.rateStarsComponent).toBeTruthy();
    });

    it('default maxRate should be 5', () => {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      expect(starsEl.length).toEqual(5);
    });

    it('should display highlighted star', () => {
      const acceptedStarsEl = fixture.debugElement
        .queryAllNodes(By.css('.accepted-rate'));
      expect(acceptedStarsEl.length).toEqual(3);
    });

    it('shouldn\'t be highlighted on mouseenter', () => {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      starsEl[0].nativeNode.dispatchEvent(new MouseEvent('mouseenter'));
      fixture.detectChanges();
      const highlightedStars = fixture.debugElement.queryAllNodes(By.css('.accepted-rate'));
      expect(highlightedStars.length).toEqual(3);
    });

    it('shouldn\'t be highlighted on mouseclick', () => {
      const starsEl = fixture.debugElement.queryAllNodes(By.css('.star'));
      starsEl[0].nativeNode.click();
      fixture.detectChanges();
      const highlightedStars = fixture.debugElement.queryAllNodes(By.css('.accepted-rate'));
      expect(highlightedStars.length).toEqual(3);
    });
  });
});

@Component({
  selector: 'test-rate-stars',
  template: `
    <app-rate-stars [maxRate]="maxRate"
                    #rateStarsComponent></app-rate-stars>`
})
class TestRateStarsFCComponent {
  public maxRate = 5;
  @ViewChild(RateStarsComponent) rateStarsComponent;

  form = this.fb.group({
    rateFC: new FormControl({ value: null, disabled: false }),
  });

  constructor(private fb: FormBuilder) {}
}

@Component({
  selector: 'test-rate-stars',
  template: `
    <app-rate-stars [staticRate]="rate"
                    #rateStarsComponent></app-rate-stars>`
})
class TestRateStarsComponent {
  public rate = 3;
  @ViewChild(RateStarsComponent) rateStarsComponent;


  constructor() {}
}
