import { ComponentFixture, inject, TestBed } from '@angular/core/testing';

import { SelectComponent } from './select.component';
import { Component, ViewChild } from '@angular/core';
import { By } from '@angular/platform-browser';
import { UiModule } from '../ui.module';
import { SelectItem } from './models';
import { FormBuilder, FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../app/material/material.module';
import { OverlayContainer } from '@angular/cdk/overlay';

describe('SelectComponent', () => {
  let component: TestFormComponent;
  let fixture: ComponentFixture<TestFormComponent>;
  let overlayContainer: OverlayContainer;
  let overlayContainerElement: HTMLElement;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiModule, FormsModule, ReactiveFormsModule, MaterialModule],
      declarations: [TestFormComponent],
    })
      .compileComponents();

    inject([OverlayContainer], (oc: OverlayContainer) => {
      overlayContainer = oc;
      overlayContainerElement = oc.getContainerElement();
    })();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not be open', () => {
    expect(fixture.componentInstance.selectComponent.isOpen).toBeFalse();
  });

  it('should display placeholder', () => {
    expect(fixture.nativeElement.querySelector('.input').placeholder).toEqual(component.placeholder);
  });

  it('should not display remove icon when value is empty', () => {
    expect(fixture.nativeElement.querySelector('.icon-wrapper')).toBeNull();
  });

  describe('click', () => {
    it('should be open', () => {
      const inputEl = fixture.debugElement.query(By.css('.input')).nativeElement;
      inputEl.click();
      fixture.detectChanges();
      const selectList = overlayContainerElement.querySelector('.select-list');
      const listItem = overlayContainerElement.querySelectorAll('li');
      expect(selectList).not.toBeNull();
      expect(listItem.length).toBe(2);
    });

    it('should display value when value is inputed', () => {
      const inputEl = fixture.debugElement.query(By.css('.input')).nativeElement;
      inputEl.click();
      fixture.detectChanges();
      const listItem = overlayContainerElement.querySelectorAll('li')[0];
      listItem.click();
      fixture.detectChanges();
      expect(component.selectComponent.value).toBe(component.items[0]);
    });

    it('should be close after input element', () => {
      const inputEl = fixture.debugElement.query(By.css('.input')).nativeElement;
      inputEl.click();
      fixture.detectChanges();
      const listItem = overlayContainerElement.querySelectorAll('li')[0];
      listItem.click();
      fixture.detectChanges();
      const selectList = overlayContainerElement.querySelector('.select-list');
      expect(selectList).toBeNull();
    });
  });

  describe('enter input', () => {
    it('should filtered out not passing phrases', () => {
      const inputEl = fixture.debugElement.query(By.css('.input')).nativeElement;
      component.selectComponent.filterProduct = 'f';
      fixture.detectChanges();
      inputEl.click();
      fixture.detectChanges();
      const listItem = overlayContainerElement.querySelectorAll('li');
      expect(listItem.length).toBe(1);
    });
  });

  describe('remove', () => {
    it('should remove inputed value and filterProduct', () => {
      const inputEl = fixture.debugElement.query(By.css('.input')).nativeElement;
      inputEl.click();
      fixture.detectChanges();
      const listItem = overlayContainerElement.querySelectorAll('li')[0];
      listItem.click();
      fixture.detectChanges();
      expect(component.selectComponent.value).toBe(component.items[0]);
      expect(component.selectComponent.filterProduct).toBe(component.items[0].text);

      // remove
      const removeIcon = fixture.debugElement.query(By.css('.icon-wrapper')).nativeElement;
      removeIcon.click();
      fixture.detectChanges();
      expect(component.selectComponent.value).toBeNull();
    });
  });
});

@Component({
  selector: 'test-select',
  template: `
    <app-select #selectComponent
                [formControl]="fc"
                [placeholder]="placeholder"
                [items]="items">
    </app-select>

  `
})
class TestFormComponent {
  @ViewChild(SelectComponent) selectComponent;
  public placeholder = 'Select me!';
  public items: SelectItem[] = [
    { id: '0', text: 'foo' }, { id: '1', text: 'bar' }
  ];
  public fc = new FormControl(null);

  constructor() {}

}

