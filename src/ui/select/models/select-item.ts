export interface SelectItem {
  id: string;
  text: string;
}
