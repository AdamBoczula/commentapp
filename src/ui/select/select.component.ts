import {
  Component,
  ElementRef, EventEmitter,
  forwardRef, HostBinding,
  HostListener,
  Input,
  OnChanges,
  OnInit, Output,
  SimpleChanges
} from '@angular/core';
import { SelectItem } from './models';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    }
  ]
})
export class SelectComponent implements ControlValueAccessor, OnChanges {
  @Input() public placeholder: string;
  @Input() public items: SelectItem[];
  @Output() public clear = new EventEmitter<void>();
  public isOpen = false;
  public filteredItems: SelectItem[];

  // tslint:disable-next-line:variable-name
  private _filterProduct: string;
  // tslint:disable-next-line:variable-name
  private _value: SelectItem;
  // tslint:disable-next-line:variable-name
  private _onChange: (val: SelectItem) => void;
  // tslint:disable-next-line:variable-name
  private _onTouch: () => void;

  constructor(private eRef: ElementRef) {}

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.items.firstChange) {
      this.filteredItems = this.items;
    }
  }

  public get value(): SelectItem {
    return this._value;
  }

  public set value(item: SelectItem) {
    this._value = item;
  }

  public get filterProduct(): string {
    return this._filterProduct;
  }

  public set filterProduct(filter: string) {
    this._filterProduct = filter;
    this.filteredItems = this.filterOutItems();
  }


  public registerOnChange(fn: any): void {
    this._onChange = fn;
  }

  public registerOnTouched(fn: any): void {
    this._onTouch = fn;
  }

  public setDisabledState(isDisabled: boolean): void {
  }

  public writeValue(item: SelectItem): void {
    this.filterProduct = item ? item.text : '';
    this.value = item;
    this.closeSelectList();
    if (item) {
      this._onChange(this.value);
    }
  }

  public selectEl(selectedItem: SelectItem): void {
    this._onTouch();
    this.writeValue(selectedItem);
  }


  @HostListener('document:click', ['$event'])
  private closeOnClickOutside(event: Event): void {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.closeSelectList();
    }
  }

  private closeSelectList(): void {
    this.isOpen = false;
  }

  private filterOutItems(): SelectItem[] {
    if (!this.filterProduct) {
      return this.items;
    }
    return this.items.filter(i => i.text.includes(this.filterProduct));
  }

  public remove(): void {
    this.clear.emit();
    if (!this.value) {
      return;
    }
    this.value = null;
  }

}
