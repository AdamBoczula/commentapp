import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './select/select.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../app/material/material.module';
import { RateStarsComponent } from './rate-stars/rate-stars.component';


@NgModule({
  declarations: [SelectComponent, RateStarsComponent],
  exports: [
    SelectComponent,
    RateStarsComponent
  ],
  imports: [
    ReactiveFormsModule,
    MaterialModule,
    CommonModule,
    FormsModule
  ]
})
export class UiModule {
}
